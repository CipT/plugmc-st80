versions
version10: spec
	<version: '1.0' imports: #('1.0-baseline' )>

	spec for: #'common' do: [
		spec blessing: #'development'.
		spec description: 'initial version'.
		spec author: 'CiprianTeodorov'.
		spec timestamp: '11/6/2015 16:48'.
		spec 
			project: 'OSProcess' with: #'stable';
			project: 'PetitParser' with: #'stable'.
		spec 
			package: 'PlugMC-BlockL-Model' with: 'PlugMC-BlockL-Model-CiprianTeodorov.1';
			package: 'PlugMC-BlockL-Runtime' with: 'PlugMC-BlockL-Runtime-CiprianTeodorov.1';
			package: 'PlugMC-Buchi-Model' with: 'PlugMC-Buchi-Model-CiprianTeodorov.1';
			package: 'PlugMC-Buchi-Runtime' with: 'PlugMC-Buchi-Runtime-CiprianTeodorov.9';
			package: 'PlugMC-Core' with: 'PlugMC-Core-CiprianTeodorov.18';
			package: 'PlugMC-Events' with: 'PlugMC-Events-CiprianTeodorov.1';
			package: 'PlugMC-Examples' with: 'PlugMC-Examples-CiprianTeodorov.2';
			package: 'PlugMC-ExpL-Eval' with: 'PlugMC-ExpL-Eval-CiprianTeodorov.1';
			package: 'PlugMC-ExpL-Model' with: 'PlugMC-ExpL-Model-CiprianTeodorov.2';
			package: 'PlugMC-ExpL-Parser' with: 'PlugMC-ExpL-Parser-CiprianTeodorov.2';
			package: 'PlugMC-IMP-Model' with: 'PlugMC-IMP-Model-CiprianTeodorov.2';
			package: 'PlugMC-IMP-Parser' with: 'PlugMC-IMP-Parser-CiprianTeodorov.4';
			package: 'PlugMC-LTL-Model' with: 'PlugMC-LTL-Model-CiprianTeodorov.2';
			package: 'PlugMC-LTL-Parser' with: 'PlugMC-LTL-Parser-CiprianTeodorov.2';
			package: 'PlugMC-LTL-Transformations' with: 'PlugMC-LTL-Transformations-CiprianTeodorov.3';
			package: 'PlugMC-LTL3BA' with: 'PlugMC-LTL3BA-CiprianTeodorov.3';
			package: 'PlugMC-Observer-Model' with: 'PlugMC-Observer-Model-CiprianTeodorov.1';
			package: 'PlugMC-Observer-Runtime' with: 'PlugMC-Observer-Runtime-CiprianTeodorov.4';
			package: 'PlugMC-Predicate-Runtime' with: 'PlugMC-Predicate-Runtime-CiprianTeodorov.1';
			package: 'PlugMC-StaL-Model' with: 'PlugMC-StaL-Model-CiprianTeodorov.2';
			package: 'PlugMC-StaL-Parser' with: 'PlugMC-StaL-Parser-CiprianTeodorov.2';
			package: 'PlugMC-StaL-Runtime' with: 'PlugMC-StaL-Runtime-CiprianTeodorov.2';
			package: 'PlugMC-TransitionSystem' with: 'PlugMC-TransitionSystem-CiprianTeodorov.2'. ].
