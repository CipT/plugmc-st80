baselines
baseline10: spec
	<version: '1.0-baseline'>

	spec for: #'common' do: [
		spec blessing: #'baseline'.
		GitRepoPath
				ifNotNil: [ spec repository: 'filetree://' , GitRepoPath ]
				ifNil: [ spec repository: 'bitbucket://CipT/plugmc-st80.git' ].
		spec 
			project: 'OSProcess' with: [
				spec
					className: 'ConfigurationOfOSProcess';
					versionString: #'stable';
					repository: 'http://smalltalkhub.com/mc/Pharo/MetaRepoForPharo40/main/' ];
			project: 'PetitParser' with: [
				spec
					className: 'ConfigurationOfPetitParser';
					versionString: #'stable';
					loads: #('Core' 'PetitAnalyzer');
					repository: 'http://smalltalkhub.com/mc/Pharo/MetaRepoForPharo40/main/' ].
		spec 
			package: 'PlugMC-BlockL-Model';
			package: 'PlugMC-BlockL-Runtime' with: [
				spec requires: #('PlugMC-Core' 'PlugMC-BlockL-Model' ). ];
			package: 'PlugMC-Buchi-Model' with: [
				spec requires: #('PlugMC-TransitionSystem' ). ];
			package: 'PlugMC-Buchi-Runtime' with: [
				spec requires: #('PlugMC-Core' 'PlugMC-Buchi-Model' ). ];
			package: 'PlugMC-Core';
			package: 'PlugMC-Events';
			package: 'PlugMC-Examples' with: [
				spec requires: #('PlugMC-Core' 'PlugMC-BlockL-Runtime' 'PlugMC-Predicate-Runtime' 'PlugMC-Observer-Runtime' 'PlugMC-Buchi-Runtime' ). ];
			package: 'PlugMC-ExpL-Eval' with: [
				spec requires: #('PlugMC-ExpL-Model' ). ];
			package: 'PlugMC-ExpL-Model';
			package: 'PlugMC-ExpL-Parser' with: [
				spec requires: #('PetitParser' 'PlugMC-ExpL-Model' ). ];
			package: 'PlugMC-IMP-Model';
			package: 'PlugMC-IMP-Parser' with: [
				spec requires: #('PetitParser' 'PlugMC-IMP-Model' 'PlugMC-ExpL-Parser' ). ];
			package: 'PlugMC-LTL-Model';
			package: 'PlugMC-LTL-Parser' with: [
				spec requires: #('PetitParser' 'PlugMC-LTL-Model' ). ];
			package: 'PlugMC-LTL-Transformations' with: [
				spec requires: #('PlugMC-LTL-Model' ). ];
			package: 'PlugMC-LTL3BA' with: [
				spec requires: #('OSProcess' 'PlugMC-LTL-Transformations' 'PlugMC-TransitionSystem' 'PlugMC-Buchi-Model' ). ];
			package: 'PlugMC-Observer-Model' with: [
				spec requires: #('PlugMC-TransitionSystem' ). ];
			package: 'PlugMC-Observer-Runtime' with: [
				spec requires: #('PlugMC-Core' 'PlugMC-Observer-Model' ). ];
			package: 'PlugMC-Predicate-Runtime' with: [
				spec requires: #('PlugMC-Core' ). ];
			package: 'PlugMC-StaL-Model';
			package: 'PlugMC-StaL-Parser' with: [
				spec requires: #('PetitParser' 'PlugMC-StaL-Model' 'PlugMC-ExpL-Parser' ). ];
			package: 'PlugMC-StaL-Runtime' with: [
				spec requires: #('PlugMC-Core' 'PlugMC-StaL-Model' ). ];
			package: 'PlugMC-TransitionSystem'.
		spec 
			group: 'default' with: #('Core' );
			group: 'Core' with: #('PlugMC-BlockL-Model' 'PlugMC-BlockL-Runtime' 'PlugMC-Buchi-Model' 'PlugMC-Buchi-Runtime' 'PlugMC-Core' 'PlugMC-Events' 'PlugMC-Examples' 'PlugMC-ExpL-Eval' 'PlugMC-ExpL-Model' 'PlugMC-ExpL-Parser' 'PlugMC-IMP-Model' 'PlugMC-IMP-Parser' 'PlugMC-LTL-Model' 'PlugMC-LTL-Parser' 'PlugMC-LTL-Transformations' 'PlugMC-LTL3BA' 'PlugMC-Observer-Model' 'PlugMC-Observer-Runtime' 'PlugMC-Predicate-Runtime' 'PlugMC-StaL-Model' 'PlugMC-StaL-Parser' 'PlugMC-StaL-Runtime' 'PlugMC-TransitionSystem' ). ].
