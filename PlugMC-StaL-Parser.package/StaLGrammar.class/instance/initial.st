as yet unclassified
initial
	^ ('initial' asParser trim , (stateReference separatedBy: $, asParser trim))
		==> [ :e | e second reject: [ :each | each = $, ] ]