as yet unclassified
update
	^ ('update' asParser trim , (assign delimitedBy: $; asParser token))
		==> [ :e | e second reject: [ :each | each = $; ] ]