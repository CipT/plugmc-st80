as yet unclassified
varDecl
	^ ('var' asParser trim , identifier , ('=' asParser trim , expression) optional)
		==> [ :e |
			{(e second).
			(e third ifNotNil: [ e third second ])} ]