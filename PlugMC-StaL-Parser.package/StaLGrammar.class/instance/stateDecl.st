as yet unclassified
stateDecl
	^'states' asParser trim, (identifier separatedBy: $, asParser trim) ==> [ :e | e second reject: [ :each | each = $, ] ]