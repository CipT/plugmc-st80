as yet unclassified
assign
	^ super assign ==> [ :v | StaLAssignment new lhs: v first; rhs: v second ]