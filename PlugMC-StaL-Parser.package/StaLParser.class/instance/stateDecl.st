as yet unclassified
stateDecl
	^ super stateDecl
		==> [ :states | states collect: [ :each | statesDict at: each asSymbol put: (StaLState new id: each asSymbol) ] ]