as yet unclassified
program
	^ super program ==> [ :elts |
		StaLProgram new
			states: elts first;
			initial: elts second;
			variables: elts third;
			transitions: elts last]