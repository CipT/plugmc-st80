as yet unclassified
transition
	^ super transition
		==> [ :v | 
			StaLTransition new
				from: v first;
				to: v second;
				guard: v third;
				update: v last ]