as yet unclassified
stateReference
	^ super stateReference
		==> [ :v | statesDict at: v asSymbol ifAbsent: [ self error: 'State named ' , v , ' is not declared.' ] ]