as yet unclassified
evaluate: anAtomicPredicate in: aState

	"for now lets say that the atomic predicate is just a block. so that we do not need to worry about compilation"
	^anAtomicPredicate value: aState.