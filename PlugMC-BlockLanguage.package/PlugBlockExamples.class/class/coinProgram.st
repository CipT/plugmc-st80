examples
coinProgram
	^ PlugBlockProgram new
		variables: (Array new: 8 withAll: 0);
		behaviors: (
		 (self coin: 1 quantity: 1)
		,(self coin: 2 quantity: 1)
		,(self coin: 3 quantity: 6)
		,(self coin: 4 quantity: 10)
		,(self coin: 5 quantity: 5)
		,(self coin: 6 quantity: 2)
		,(self coin: 7 quantity: 1)
		,(self coin: 8 quantity: 0)
		)