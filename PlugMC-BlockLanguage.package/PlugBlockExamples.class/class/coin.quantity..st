examples
coin: id quantity: quantity
	| a b |
	a := PlugBlockBehavior guard: [ :env | (env at: id) < quantity ] action: [ :env | env at: id put: (env at: id) + 1 ].
	b := PlugBlockBehavior guard: [ :env | (env at: id) >= quantity ] action: [ :env | env at: id put: 0 ].
	^{a. b}