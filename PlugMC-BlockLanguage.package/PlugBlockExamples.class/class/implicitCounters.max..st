examples
implicitCounters: nbCounters max: maxValue
	|behaviors|
	behaviors := (1 to: nbCounters) collect: [ :idx | 
		PlugBlockBehavior 
			guard: [ :env | true ] 
			action: [ :env | env at: idx put: ((env at: idx) + 1) \\ maxValue ] ].

	^ PlugBlockProgram new
		variables: (Array new: nbCounters withAll: 0);
		behaviors: behaviors.