examples
explicitCounters: nbCounters max: maxValue
	|behaviors|
	behaviors := (1 to: nbCounters) collect: [ :idx | |inc reset|
		inc := PlugBlockBehavior 
			guard: [ :env | (env at: idx) <= maxValue ] 
			action: [ :env | env at: idx put: (env at: idx) + 1 ].
		reset := PlugBlockBehavior 
			guard: [:env | (env at: idx) >= maxValue ]
			action: [ :env | env at: idx put: 0 ].
		{ inc. reset } ].

	^ PlugBlockProgram new
		variables: (Array new: nbCounters withAll: 0);
		behaviors: behaviors flatten.