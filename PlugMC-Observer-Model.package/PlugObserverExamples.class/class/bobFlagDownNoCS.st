as yet unclassified
bobFlagDownNoCS

	|start down reject s2d d2s d2r a sortBlock|
	start := PlugObserverState new id: #start.
	down := PlugObserverState new id: #down.
	reject := PlugObserverState reject.
	
	s2d := (PlugObserverTransition from: start to: down guard: [ :p | p not ] atoms: { [:e | (e at: #flagBob)] }) priority: 1.
	d2s := (PlugObserverTransition from: down to: start guard: [ :p | p ] atoms: { [:e | (e at: #flagBob)]  }) priority: 1.
	d2r := (PlugObserverTransition from: down to: reject guard: [ :p | p ] atoms: { [ :e | (e at: #bob) = #dogInYard ] }) priority: 2.
	
	sortBlock := [:x :y | x priority < y priority].
	
	a := PlugTAutomaton new
		initial: start.
	a fanout at: start put: ({ s2d } sorted: sortBlock).
	a fanout at: down put: ({ d2s. d2r } sorted: sortBlock).
	^a