as yet unclassified
fireableTransitionsFrom: aState to: target runtime: languageRuntime
	"this should be used for state-based verification environments (observer, buchi) that need to synchronously advance along the system"
	"here I suppose that the transitions are already sorted in priority order"
	observer
		fireableTransitionsIn: (aState observers at: observerIdx)
		do: [ :fireableTransition | 
			(self isEnabled: fireableTransition in: target systemState runtime: languageRuntime)
				ifTrue: [ 
					target observers at: observerIdx put: fireableTransition to.
					^ target
					 ] ].
	target observers at: observerIdx put: (aState observers at: observerIdx).
	^target