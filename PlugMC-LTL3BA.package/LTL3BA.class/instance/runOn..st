as yet unclassified
runOn: aLTLFormula
	|env path text baString|
	
	text := aLTLFormula accept: LTL2AbstractFormulaText new.
	
	env := OSProcess thisOSProcess environment copy.
	path := (env at: #PATH), ':', self class ltl3baPath.  "(SmalltalkImage current imageDirectory / '../../../ltl3ba-1.0.2/')  fullName."
	env at: #PATH put: path.
	baString := (PipeableOSProcess command: 'ltl3ba -U -f "',text,'"' environment: env)
		output.
		
	^self readBuchi: baString readStream context: aLTLFormula atoms.
	