A LTL3BA is xxxxxxxxx.

formula := LTLExpression2LTLFormula from: ( PlugLTLParser parse: 'ltl ((G "flagA" -> F "a.CS") && (G "flagB" -> F "b.CS"))').
LTL3BA new
	runOn: formula