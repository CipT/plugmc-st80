as yet unclassified
ltl3baPath
	^ ltl3baPath
		ifNil: [ ltl3baPath := self checkLtl3baPath: '/bin' asFileReference ]
		ifNotNil: [ self checkLtl3baPath: ltl3baPath asFileReference ]