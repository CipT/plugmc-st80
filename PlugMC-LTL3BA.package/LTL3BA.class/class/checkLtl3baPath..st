as yet unclassified
checkLtl3baPath: aPath
	| ltl3baExe |
	ltl3baExe := aPath / 'ltl3ba'.
	((aPath / 'ltl3ba') exists and: [ OSProcess accessor isExecutable: ltl3baExe fullName ])
		ifFalse: [ ^ ltl3baPath := (UIManager default chooseDirectory: 'Choose ltl3ba binary path') fullName ].
	^ aPath fullName