A PlugSpotBuchiReader converts a textual buchi automata obtained from ltl3ba to a Buchi Automata in the PlugMC-TransitionSystem language. The guards use the LTLExpression syntax and should be interpreted as such.


formula := LTLExpression2LTLFormula from: ( PlugLTLParser parse:  'ltl !((G "a" -> F "b") && (G "c" -> F "d"))').
baString := 'acc = "1";
accept_init, accept_S2, "(!atom4 && atom3)", "1";
accept_init, accept_S1, "(!atom2 && atom1)", "1";
accept_S1, accept_S1, "(!atom2 && atom1)", "1";
accept_S2, accept_S2, "(!atom4 && atom3)", "1";'.
PlugSpotBuchiReader new
	readBuchi: baString readStream context: formula atoms