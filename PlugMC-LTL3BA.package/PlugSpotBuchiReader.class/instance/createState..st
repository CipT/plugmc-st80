as yet unclassified
createState: aName
	^ (aName beginsWith: 'accept')
		ifTrue: [ PlugBuchiState accepting id: aName ]
		ifFalse: [ PlugBuchiState new id: aName ]