as yet unclassified
readBuchi: stream context: atomDictionary
	|transitions|
	"discard acc='1'"
	(stream upTo: $;) trim.
	transitions := OrderedCollection new.
	[ stream atEnd ] whileFalse: [ |source target guardString |
		source := (stream upTo: $,) trim asSymbol.
		target := (stream upTo: $,) trim asSymbol.
		guardString := (((stream upTo: $,) trim) trimBoth: [:char | char = $"]) asSymbol.
		guardString = '(1)' ifTrue: [ guardString := 'true' ].
		"drop the accepting transition"
		(stream upTo: $;).
		(source isEmpty & (target isEmpty)) ifFalse: [
		transitions add: { source. target. guardString}].
	].
	^self buchi: transitions context: atomDictionary