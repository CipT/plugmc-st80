as yet unclassified
buchi: transitionList context: atomDictionary
	| name2state stateFanout initial |
	name2state := Dictionary new.
	stateFanout := IdentityDictionary new.
	transitionList
		do: [ :each | 
			| source target guard |
			source := name2state
				at: each first
				ifAbsentPut: [ 
					| bstate |
					bstate := self createState: each first.
					stateFanout at: bstate put: Set new.
					(each first endsWith: 'init')
						ifTrue: [ initial := bstate ].
					bstate ].
			target := name2state
				at: each second
				ifAbsentPut: [ 
					| bstate |
					bstate := self createState: each second.
					stateFanout at: bstate put: Set new.
					bstate ].
			guard := LTLExpression2LTLFormula from: (PlugLTLParser parse: 'ltl ' , each third) inContext: atomDictionary.
			(stateFanout at: source)
				add:
					(PlugTTransition
						from: source
						to: target
						guard: guard
						atoms: {}) ].
	^ PlugTAutomaton new
		initial: initial;
		fanout: stateFanout