as yet unclassified
evaluate: aPredicate in: aConfiguration runtime: languageRuntime
	"for now the predicates should be expressed in the language runtime language.
	and are completely handled by it."
	"in the future, for some languages that have poor expressivity power we might implement a dedicated language for predicates, which uses only atoms from the language runtime.
	In that case the predicate evaluation will be implemented by the predicate language runtime delegating the atom computing to the language runtime."
	^languageRuntime evaluate: aPredicate in: aConfiguration systemState