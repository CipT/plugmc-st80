as yet unclassified
expression
	| expr binary unary|
	expr := PPExpressionParser new.
	expr term: parens / literals.
	
	unary := [ :g :ops | ops do: [ :opParser | g prefix: opParser do: self unaryBlock ] ].
	binary := [ :g :ops | ops do: [:opParser | g left: opParser do: self binaryBlock ] ].
	
	expr
		group: [ :g | 
			unary value: g value: { $! asParser trim }];
		group: [ :g |
			binary value: g value: { $% asParser trim }];
		group: [ :g |
			binary value: g value: { $+ asParser trim. $- asParser trim }];
		group: [ :g |
			binary value: g value: { '<' asParser trim. '<=' asParser trim. '>' asParser trim. '>=' asParser trim }];
		group: [ :g |
			binary value: g value: { '==' asParser trim. '<>' asParser trim }];
		group: [ :g |
			binary value: g value: { '&&' asParser trim. '||' asParser trim }].
	^expr