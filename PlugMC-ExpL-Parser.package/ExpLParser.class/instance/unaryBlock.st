as yet unclassified
unaryBlock
	^[:op :a | op = $! ifTrue: [ ExpLNegation new operand: a ] ifFalse: [ self error: 'unexpected unary operator' ] ]