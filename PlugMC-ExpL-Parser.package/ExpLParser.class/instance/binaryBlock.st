as yet unclassified
binaryBlock
	^ [ :a :op :b | 
	| oper |
	oper := self class binaryOperators
		at: op
		ifAbsent: [ 
			self error: 'unexpected binary operator'.
			nil ].
	oper new lhs: a rhs: b ]