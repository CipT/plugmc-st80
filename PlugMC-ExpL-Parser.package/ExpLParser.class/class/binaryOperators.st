as yet unclassified
binaryOperators
	^{ '%' -> ExpLMod.
	'+' -> ExpLPlus.
	'-' -> ExpLMinus.
	'==' -> ExpLEq.
	'<>' -> ExpLNeq.
	'&&' -> ExpLAnd.
	'||' -> ExpLOr.
	'<' -> ExpLLT.
	'<=' -> ExpLLE.
	'>' -> ExpLGT.
	'>=' -> ExpLGE} asDictionary 