as yet unclassified
from: aState to: anotherState guard: guardBlock atoms: listOfAtoms
	^ self new
		from: aState;
		to: anotherState;
		guard: guardBlock;
		atoms: listOfAtoms