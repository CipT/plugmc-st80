accessing
fireableTransitionsIn: aState do: aBlock
	(fanout at: aState ifAbsent: [ #() ]) do: [ :each | aBlock value: each ].