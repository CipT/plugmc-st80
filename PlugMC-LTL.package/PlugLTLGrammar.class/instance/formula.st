as yet unclassified
formula
	|expression binary unary|
	expression := PPExpressionParser new.
	expression term: parens / literals.
	
	unary := [ :g :ops | ops do: [ :opParser | g prefix: opParser do: self unaryBlock ] ].
	binary := [ :g :ops | ops do: [:opParser | g left: opParser do: self binaryBlock ] ].
	
	expression
		group: [ :g | 
			unary value: g value: { notOp }];
		group: [ :g | 
			unary value: g value: { next }];
		group: [ :g | 
			unary value: g value: { future }];
		group: [ :g | 
			unary value: g value: { globally }];
		group: [ :g | 
			binary value: g value: { until }];
		group: [ :g | 
			binary value: g value: { release }];
		group: [ :g | 
			binary value: g value: { weakuntil }];
		group: [ :g |
			binary value: g value: { andOp }];
		group: [ :g |
			binary value: g value: { orOp }];
		group: [ :g |
			binary value: g value: { implies }].
	
	^expression