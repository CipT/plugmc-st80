accessing
literals
	
	^ 'true' asParser
	/ 'false' asParser
	/ identifier
	/ atomInline