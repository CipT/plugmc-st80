A PlugLTLGrammar is a simple LTL parser.

Instance Variables
	andOp:		<Object>
	atomInline:		<Object>
	char:		<Object>
	charEscape:		<Object>
	charHex:		<Object>
	charNormal:		<Object>
	formula:		<Object>
	future:		<Object>
	globally:		<Object>
	identifier:		<Object>
	implies:		<Object>
	literals:		<Object>
	ltl:		<Object>
	next:		<Object>
	notOp:		<Object>
	orOp:		<Object>
	parens:		<Object>
	release:		<Object>
	until:		<Object>
	weakuntil:		<Object>

andOp
	- xxxxx

atomInline
	- xxxxx

char
	- xxxxx

charEscape
	- xxxxx

charHex
	- xxxxx

charNormal
	- xxxxx

formula
	- xxxxx

future
	- xxxxx

globally
	- xxxxx

identifier
	- xxxxx

implies
	- xxxxx

literals
	- xxxxx

ltl
	- xxxxx

next
	- xxxxx

notOp
	- xxxxx

orOp
	- xxxxx

parens
	- xxxxx

release
	- xxxxx

until
	- xxxxx

weakuntil
	- xxxxx
