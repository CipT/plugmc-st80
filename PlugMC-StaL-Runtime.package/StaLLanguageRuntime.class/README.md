A StaLLanguageRuntime is 


program := StaLGrammar parse: 'states a.idle, a.wait, a.CS, b.idle, b.checkA, b.wait, b.CS
initial a.idle, b.idle
var flagA = false
var flagB = false

a.idle -> a.wait 
	update flagA = true
a.wait -> a.CS
	guard !flagB
a.CS -> a.idle
	update flagA = false

b.idle -> b.checkA
	update flagB = true
b.checkA -> b.wait
	guard flagA
	update flagB = false
b.checkA -> b.CS
	guard !flagA
b.wait -> b.CS
	guard !flagA
	update flagB = true
b.CS -> b.idle
	update flagB = false'.
StaLLanguageRuntime new
	program: program;
	compile



Instance Variables
	behaviors:		<Object>
	program:		<Object>
	stateIdx:		<Object>

behaviors
	- xxxxx

program
	- xxxxx

stateIdx
	- xxxxx
