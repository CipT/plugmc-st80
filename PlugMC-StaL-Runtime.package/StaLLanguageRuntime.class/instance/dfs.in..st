as yet unclassified
dfs: aState in: aSet
	| transitions |
	transitions := program transitions select: [ :each | each from == aState ].
	transitions do: [ :transition | aSet add: transition ifAbsent: [ self dfs: transition to in: aSet ] ]