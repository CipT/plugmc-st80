as yet unclassified
compile
	|automata|
	automata := program initial collect: [ :each | |behavior|
		behavior := IdentitySet new.
		self dfs: each in: behavior.
		each -> behavior
	].