as yet unclassified
stateReference
	^ super stateReference ==> [ :v | prg states at: v inputValue asSymbol ifAbsent: [self error: 'State named ', v inputValue, ' is not declared.' ] ]