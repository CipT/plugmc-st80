as yet unclassified
varReference
	^ super varReference ==> [ :v | 
		prg variables at: v inputValue asSymbol 
			ifPresent: [ :p | StatesVarReference new id: v inputValue asSymbol; references: p ] 
			ifAbsent: [self error: 'Variable named ', v inputValue, ' is not declared.' ] ]