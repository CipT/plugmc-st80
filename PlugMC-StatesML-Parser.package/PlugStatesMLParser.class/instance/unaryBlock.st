as yet unclassified
unaryBlock
	^[:op :a | op = $! ifTrue: [ StatesNegationExpression new operand: a ] ifFalse: [ self error: 'unexpected unary operator' ] ]