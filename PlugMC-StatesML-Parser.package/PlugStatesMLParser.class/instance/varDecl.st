as yet unclassified
varDecl
	^ super varDecl ==> [ :each | prg variables at: each first inputValue asSymbol put: (StatesVarDeclaration new id: each first inputValue asSymbol; initial: each second) ]