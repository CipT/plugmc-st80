as yet unclassified
assign
	^ super assign ==> [ :v | StatesAssignStmt new lhs: v first; rhs: v second ]