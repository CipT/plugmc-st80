as yet unclassified
stateDecl
	^ super stateDecl
		==> [ :states | states do: [ :each | prg states at: each inputValue asSymbol put: (PlugTState new id: each inputValue asSymbol) ] ]