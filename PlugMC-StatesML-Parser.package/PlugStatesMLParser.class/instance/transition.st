as yet unclassified
transition
	^ super transition
		==> [ :v | 
			| t dic |
			t := StatesTransition new
				from: v first;
				to: v second;
				guard: v third;
				updateB: v last.
			dic := prg automaton fanout.
			dic at: v first ifPresent: [ :p | p add: t ] ifAbsent: [ dic at: v first put: {t} asSet ] ]