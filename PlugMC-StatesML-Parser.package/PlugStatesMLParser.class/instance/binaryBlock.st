as yet unclassified
binaryBlock
	^ [ :a :op :b | 
	op = $%
		ifTrue: [ StatesDivExpression new lhs: a rhs: b ]
		ifFalse: [ 
			op = $+
				ifTrue: [ StatesPlusExpression new lhs: a rhs: b ]
				ifFalse: [ 
					op = $-
						ifTrue: [ StatesMinusExpression new lhs: a rhs: b ]
						ifFalse: [ 
							op = '=='
								ifTrue: [ StatesEqExpression new lhs: a rhs: b ]
								ifFalse: [ 
									op = '<>'
										ifTrue: [ StatesNeqExpression new lhs: a rhs: b ]
										ifFalse: [ 
											op = '&&'
												ifTrue: [ StatesAndExpression new lhs: a rhs: b ]
												ifFalse: [ 
													op = '||'
														ifTrue: [ StatesOrExpression new lhs: a rhs: b ]
														ifFalse: [ self error: 'unexpected unary operator' ] ] ] ] ] ] ] ]