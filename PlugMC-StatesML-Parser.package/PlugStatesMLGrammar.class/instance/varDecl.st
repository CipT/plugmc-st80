as yet unclassified
varDecl
	^'var' asParser trim, identifier, (':=' asParser trim, literals) optional ==> [ :e | {e second. e third ifNotNil: [ e third second ] }  ]