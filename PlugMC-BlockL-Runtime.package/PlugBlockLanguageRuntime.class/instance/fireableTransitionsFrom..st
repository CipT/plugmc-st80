as yet unclassified
fireableTransitionsFrom: aState
	^ (program behaviors
		collect: [ :behavior | 
			(behavior isEnabled: aState)
				ifTrue: [ 
					| target |
					target := aState deepCopy.
					behavior runInEnv: target.
					explorer newConfiguration systemState: target ]
				ifFalse: [ nil ] ]) reject: [ :each | each isNil ]