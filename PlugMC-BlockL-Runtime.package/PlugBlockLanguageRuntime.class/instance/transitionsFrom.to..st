as yet unclassified
transitionsFrom: aState to: anotherState
	^ ((program behaviors
		collect: [ :behavior | 
			(behavior isEnabled: aState)
				ifTrue: [ 
					| target |
					target := aState deepCopy.
					behavior runInEnv: target.
					target -> behavior ]
				ifFalse: [ nil -> behavior ] ]) select: [ :each | each key = anotherState ]) collect: #value