as yet unclassified
operator: symbol operand: operand
	| class |
	symbol = #negation
		ifTrue: [ class := LTLLogicalNegation ].
	symbol = #eventually
		ifTrue: [ class := LTLEventually ].
	symbol = #globally
		ifTrue: [ class := LTLGlobally ].
	symbol = #next
		ifTrue: [ class := LTLNext ].
	class ifNil: [ self error: 'Operator ' , symbol , ' not supported' ].
	^ class operand: operand