as yet unclassified
lhs: lhs operator: symbol rhs: rhs
	| class |
	symbol = #conjunction
		ifTrue: [ class := LTLLogicalConjunction ].
	symbol = #disjunction
		ifTrue: [ class := LTLLogicalDisjunction ].
	symbol = #equivalence
		ifTrue: [ class := LTLLogicalEquivalence ].
	symbol = #implication
		ifTrue: [ class := LTLLogicalImplication ].
	symbol = #release
		ifTrue: [ class := LTLRelease ].
	symbol = #until
		ifTrue: [ class := LTLUntil ].
	class ifNil: [ self error: 'Operator ' , symbol , ' not supported' ].
	^ class lhs: lhs rhs: rhs