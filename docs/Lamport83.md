###Lamport Mutual Exclusion Algorithm
####Imp Language
```
var flagA = false
var flagB = false
A: while true
	do 
		flagA = true;
		while flagB do skip od;
		dog :@ yard;
		flagA = false
	od;

B: while true 
	do
		flagB = true;
		while flagA do
			flagB = false;
			while flagA do skip od;
			flagB = true
		od;
		dog :@ yard;
		flagB = false
	od
```

####States Language
```
states a.idle, a.wait, a.CS, b.idle, b.checkA, b.wait, b.CS
initial a.idle, b.idlevar flagA = falsevar flagB = falsea.idle -> a.wait 	update flagA = truea.wait -> a.CS	guard !flagBa.CS -> a.idle	update flagA = falseb.idle -> b.checkA	update flagB = trueb.checkA -> b.wait	guard flagA	update flagB = falseb.checkA -> b.CS	guard !flagAb.wait -> b.CS	guard !flagA	update flagB = trueb.CS -> b.idle	update flagB = false```
###Properties
####Absence of deadlock>**ltl** [] !deadlock ####Mutual exclusion>**ltl** [] !(a.CS and b.CS)
>**predicate** !(a.CS and b.CS)####FairnessIf flagA or flagB are set, then eventually  Alice or Bob enter CS respectively>**ltl** ([] (flagA => <> a.CS)) and ([] (flagB => <> b.CS))####IdlingIf the flag is not set then no CS access>**ltl** ([] !flagA => ([] !a.CS)) and ([] !flagB => ([] !b.CS))
```	observer AliceIdle { 		start -> down guard !flagA;		down -> start guard flagA;		down -> reject guard a.CS }	and	observer BogIdle { 		start -> down guard !flagB;		down -> start guard flagB;		down -> reject guard b.CS }
```	####InfoftenProcesses enter CS infinitely often>**ltl** ([] <> (a.CS or b.CS))####Eventually CSEither process can eventually enter the CS>**ltl** <> a.CS and <> b.CS