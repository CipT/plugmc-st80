package JICPropertyLanguage : JICPropertyLanguage = 'JICPL'
{
	package model : model = 'model'
	{
		abstract class Element;
		abstract class NamedElement
		{
			attribute name : String;
		}
		class Atom extends NamedElement
		{
			attribute value : String;
		}
		abstract class Expression extends Element;
		class AtomReference extends Expression
		{
			property atom : Atom;
		}
		abstract class Literal extends Expression;
		abstract class BooleanLiteral extends Literal;
		class TrueLiteral extends BooleanLiteral;
		class FalseLiteral extends BooleanLiteral;
		abstract class BinaryExpression extends Expression
		{
			property lhs : Expression { composes };
			property rhs : Expression { composes };
		}
		class LogicalConjunction extends BinaryExpression;
		class LogicalDisjunction extends BinaryExpression;
		abstract class UnaryExpression extends Expression
		{
			property operand : Expression { composes };
		}
		class LogicalNegation extends UnaryExpression;
		class State extends NamedElement;
		class RejectState extends NamedElement;
		class Transition extends Element
		{
			property from : State;
			property to : State;
			property guard : Expression { composes };
		}
		abstract class Automata extends NamedElement
		{
			property states : State[*] { composes };
			property initialState : State;
			property transitions : Transition[*] { ordered composes };
		}
		class ObserverAutomata extends Automata;
		class Predicate extends NamedElement
		{
			property condition : Expression { composes };
		}
		class PropertySet extends NamedElement
		{
			property atoms : Atom[*] { composes };
			property observers : ObserverAutomata[*] { composes };
			property predicates : Predicate[*] { composes };
		}
	}
}