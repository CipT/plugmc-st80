//This file was generated from (Academic) UPPAAL 4.0.13 (rev. 4577), September 2010

/*

*/
A<> (Alice.CS or Bob.CS)

/*
if the flagB is not set then Bob cannot get to the critical section
*/
!flagB --> !Bob.CS

/*
if the flagA is not set then Alice cannot get to the critical section
*/
!flagA --> !Alice.CS

/*
flagB leads to Bob in the critical section. whenever flagB is set eventually Bob gets to the critical section. THIS IS FALSE for Lamport algorithm since the algorithm is not fair
*/
flagB --> Bob.CS

/*
flagA leads to Alice in the critical section. whenever flagA is set eventually alice gets to the critical section
*/
flagA --> Alice.CS

/*
it is possible for bob to get to the critical section
*/
E<>Bob.CS

/*
it is possible for alice to get to the critical section
*/
E<>Alice.CS

/*
the absence of deadlocks
*/
A[] !deadlock

/*
the mutual exclusion property. we cannot Alice and Bob in the critical section at the same time
*/
A[] !(Alice.CS and Bob.CS)
