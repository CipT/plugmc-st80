Creator	"yFiles"
Version	"2.12"
graph
[
	hierarchic	1
	label	""
	directed	1
	node
	[
		id	0
		label	"PlugMC-BlockL-Model"
		graphics
		[
			x	109.3079365079365
			y	-566.8662710511035
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-BlockL-Model"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	1
		label	"PlugMC-BlockL-Runtime"
		graphics
		[
			x	156.46190476190475
			y	-257.71190476190475
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-BlockL-Runtime"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	2
		label	"PlugMC-Buchi-Model"
		graphics
		[
			x	998.7714285714286
			y	-566.8662710511035
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-Buchi-Model"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	3
		label	"PlugMC-Buchi-Runtime"
		graphics
		[
			x	812.3095238095239
			y	-257.71190476190475
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-Buchi-Runtime"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	4
		label	"PlugMC-Core"
		graphics
		[
			x	593.6936507936508
			y	-566.8662710511035
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-Core"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	5
		label	"PlugMC-Events"
		graphics
		[
			x	796.7277390631048
			y	-811.8662710511035
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-Events"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	6
		label	"PlugMC-Examples"
		graphics
		[
			x	661.3842237708091
			y	-27.245934959349597
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-Examples"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	7
		label	"PlugMC-ExpL-Model"
		graphics
		[
			x	1873.2349206349204
			y	-811.8662710511035
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-ExpL-Model"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	8
		label	"PlugMC-ExpL-Parser"
		graphics
		[
			x	1873.2349206349204
			y	-566.8662710511035
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-ExpL-Parser"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	9
		label	"PlugMC-ExpL-Eval"
		graphics
		[
			x	2091.8507936507935
			y	-566.8662710511035
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-ExpL-Eval"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	10
		label	"PlugMC-IMP-Model"
		graphics
		[
			x	2310.4666666666667
			y	-566.8662710511035
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-IMP-Model"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	11
		label	"PlugMC-IMP-Parser"
		graphics
		[
			x	2154.004761904762
			y	-257.71190476190475
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-IMP-Parser"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	12
		label	"PlugMC-LTL-Model"
		graphics
		[
			x	1436.0031746031746
			y	-811.8662710511035
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-LTL-Model"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	13
		label	"PlugMC-LTL-Parser"
		graphics
		[
			x	1217.3873015873014
			y	-566.8662710511035
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-LTL-Parser"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	14
		label	"PlugMC-LTL-Transformations"
		graphics
		[
			x	1436.0031746031746
			y	-566.8662710511035
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-LTL-Transformations"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	15
		label	"PlugMC-LTL3BA"
		graphics
		[
			x	1108.079365079365
			y	-257.71190476190475
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-LTL3BA"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	16
		label	"PlugMC-Observer-Model"
		graphics
		[
			x	878.9908826945411
			y	-664.8337507259002
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-Observer-Model"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	17
		label	"PlugMC-Observer-Runtime"
		graphics
		[
			x	593.6936507936508
			y	-257.71190476190475
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-Observer-Runtime"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	18
		label	"PlugMC-Predicate-Runtime"
		graphics
		[
			x	375.0777777777778
			y	-257.71190476190475
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-Predicate-Runtime"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	19
		label	"PlugMC-StaL-Model"
		graphics
		[
			x	1654.6190476190477
			y	-566.8662710511035
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-StaL-Model"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	20
		label	"PlugMC-StaL-Parser"
		graphics
		[
			x	1778.9269841269838
			y	-257.71190476190475
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-StaL-Parser"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	21
		label	"PlugMC-StaL-Runtime"
		graphics
		[
			x	1560.3111111111111
			y	-257.71190476190475
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-StaL-Runtime"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	22
		label	"PlugMC-TransitionSystem"
		graphics
		[
			x	1060.9253968253968
			y	-811.8662710511035
			w	188.61585365853648
			h	30.0
			type	"rectangle"
			hasFill	0
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"PlugMC-TransitionSystem"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	edge
	[
		source	1
		target	4
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	1
		target	0
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	2
		target	22
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	3
		target	4
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	3
		target	2
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	6
		target	4
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	6
		target	1
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	6
		target	18
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	6
		target	17
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	6
		target	3
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	8
		target	7
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	9
		target	7
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	11
		target	10
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	11
		target	8
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	13
		target	12
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	14
		target	12
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	15
		target	14
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	15
		target	22
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	15
		target	2
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	16
		target	22
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	17
		target	4
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	17
		target	16
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	18
		target	4
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	20
		target	19
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	20
		target	8
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	21
		target	4
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	21
		target	19
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
]
