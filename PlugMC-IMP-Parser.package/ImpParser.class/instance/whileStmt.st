as yet unclassified
whileStmt
	^ super whileStmt
		==> [ :e | 
			ImpWhile new
				condition: e first;
				statements: e second ]