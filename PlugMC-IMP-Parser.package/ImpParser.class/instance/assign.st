as yet unclassified
assign
	^ super assign ==> [ :v | ImpAssignment new lhs: v first; rhs: v second ]