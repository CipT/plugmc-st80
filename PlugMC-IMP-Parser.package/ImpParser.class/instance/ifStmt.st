as yet unclassified
ifStmt
	^ super ifStmt
		==> [ :elts | 
			ImpIf new
				condition: elts first;
				trueBranch: elts second;
				falseBranch: elts third ]