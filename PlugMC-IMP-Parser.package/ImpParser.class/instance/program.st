as yet unclassified
program
	^ super program ==> [ :e | ImpProgram new
			variables: e first;
			statements: e second ]