as yet unclassified
varReference
	^ super varReference ==> [ :v |
		variablesDict at: v asSymbol 
			ifPresent: [ :p | ExpLVarReference new id: v asSymbol; reference: p ] 
			ifAbsent: [self error: 'Variable named ', v, ' is not declared.' ] ]