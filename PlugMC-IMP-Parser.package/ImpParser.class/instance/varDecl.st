as yet unclassified
varDecl
	^ super varDecl
		==> [ :each |
			variablesDict
				at: each first asSymbol
				put:
					(ImpVarDeclaration new
						id: each first asSymbol;
						initial: each second) ]