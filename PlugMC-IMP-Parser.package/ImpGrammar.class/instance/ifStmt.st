as yet unclassified
ifStmt
	^'if' asParser trim, 
	expression, 
	'then' asParser trim, 
	statements, 
	('else' asParser trim, statements) optional, 
	'fi' asParser trim ==> [ :v | { v second. v at: 4. (v at: 5) ifNotNil: [ (v at: 5) second ] } ]