as yet unclassified
statement
	^ (identifier , ':' asParser trim) optional , (ifStmt / whileStmt / assign / skipStmt / markerStmt) ==> [ :e | 
		{ e first ifNotNil: [ e first first ]. e second } ]