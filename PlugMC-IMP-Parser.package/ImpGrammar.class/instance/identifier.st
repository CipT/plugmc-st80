as yet unclassified
identifier
	^ ((reservedWord , #space asParser) not and , (#letter asParser , ($. asParser / #word asParser) star) token trim)
		==> [ :e | e last inputValue ]