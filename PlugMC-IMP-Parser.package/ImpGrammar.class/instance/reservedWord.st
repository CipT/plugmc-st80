as yet unclassified
reservedWord
	^'skip' asParser 
	/ 'while' asParser 
	/ 'do' asParser 
	/ 'od' asParser 
	/ 'if' asParser 
	/ 'then' asParser 
	/ 'else' asParser 
	/ 'fi' asParser 
	/ 'var' asParser 
