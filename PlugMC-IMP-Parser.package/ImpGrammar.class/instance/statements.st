as yet unclassified
statements
	^(statement delimitedBy: $; asParser trim) ==> [ :e | e reject: [ :each | each = $; ] ]