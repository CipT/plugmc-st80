as yet unclassified
expressionGrammar
	| expP |
	expP := self dependencyAt: ExpLGrammar.
	(expP productionAt: #identifier) replace: (expP productionAt: #identifier) children first with: identifier.
	^expP
	