visitor
visitAtomReference: aNode
	|name|
	aNode atomName ifNotNil: [ ^aNode atomName ].
	name := aNode reference accept: self.
	aNode atomName: name.
	^name