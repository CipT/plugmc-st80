visitor
visitGlobally: aNode
	|operand|
	operand := aNode operand accept: self.
	^'([] ', operand, ')'