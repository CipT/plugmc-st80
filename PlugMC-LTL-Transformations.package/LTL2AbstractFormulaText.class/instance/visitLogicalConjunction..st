visitor
visitLogicalConjunction: aNode
	|lhs rhs|
	lhs := aNode lhs accept: self.
	rhs := aNode rhs accept: self.
	^'(', lhs, ' && ', rhs , ')'