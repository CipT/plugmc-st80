visitor
visitLogicalNegation: aNode
	|operand|
	operand := aNode operand accept: self.
	^'(!', operand, ')'