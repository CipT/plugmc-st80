visitor
visitNext: aNode
	|operand|
	operand := aNode operand accept: self.
	^'(X ', operand, ')'