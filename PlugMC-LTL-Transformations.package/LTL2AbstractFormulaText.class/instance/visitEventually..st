visitor
visitEventually: aNode
	|operand|
	operand := aNode operand accept: self.
	^'(<> ', operand, ')'