as yet unclassified
from: aLTLExpression inContext: aName2AtomMap
	^ self new
		name2atom: aName2AtomMap;
		formulaFrom: aLTLExpression
