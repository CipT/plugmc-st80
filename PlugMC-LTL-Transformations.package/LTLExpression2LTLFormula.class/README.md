A LTLExpression2LTLFormula is extracting the inline atoms from an LTL expression. it is also able to build a formula using a given atom binding from a formula with atoms identified by name.

see the next example

|expression formula text expression1 |
expression := PlugLTLParser parse: 'ltl GF"x"'.
formula := LTLExpression2LTLFormula from: expression.
text := formula accept: LTL2AbstractFormula new.
expression1 := PlugLTLParser parse: 'ltl ', text.  
formula1 := LTLExpression2LTLFormula from: expression1 inContext: formula atoms 



Instance Variables
	atom2name:		<Object>
	id:		<Object>
	name2atom:		<Object>

atom2name
	- xxxxx

id
	- xxxxx

name2atom
	- xxxxx
