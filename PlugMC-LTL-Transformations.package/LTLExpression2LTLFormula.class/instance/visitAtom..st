visitor
visitAtom: aNode
	|atomName atomRef |
	atomName := atom2name at: aNode ifAbsentPut: [ | name |
		id := id + 1.
		name :=  'atom', id printString.
		name ].
	
	atomRef := LTLAtomReference named: atomName.
	aNode become: atomRef.
	aNode reference: atomRef.
	name2atom at: atomName put: atomRef.