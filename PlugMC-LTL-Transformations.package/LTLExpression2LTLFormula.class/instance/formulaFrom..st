as yet unclassified
formulaFrom: aLTLExpression
	aLTLExpression accept: self.
	^LTLFormula new
		atoms: name2atom;
		expression: aLTLExpression
		