accessing
name2atom: anObject
	
	name2atom := anObject.
	name2atom keysAndValuesDo: [ :key :value | atom2name at: value put: key ]