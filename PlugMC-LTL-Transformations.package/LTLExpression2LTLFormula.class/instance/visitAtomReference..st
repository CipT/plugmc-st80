visitor
visitAtomReference: aNode
	aNode reference
		ifNil: [ 
			aNode
				reference:
					(name2atom
						at: aNode atomName
						ifAbsent: [ 
							self error: 'missing atom named ' , aNode atomName.
							nil ]) ]