visitor
visitBinaryExpression: aNode
	aNode lhs accept: self.
	aNode rhs accept: self.