as yet unclassified
counterResetBFSComposition
	"			self counterResetDFSComposition		"
|explorer|
explorer := PlugBFSExplorer new.
explorer
	runtime: (PlugBlockLanguageRuntime new 
				explorer: explorer;
				program: PlugBlockExamples counterReset);
	configurationManager: TracingConfigurationManager new;
	transitionManager: CountingTransitionManager new;
	explore.
	^explorer