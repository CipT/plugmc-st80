as yet unclassified
lamport83infinitelyOften
	"			self lamport83infinitelyOften			"

	| buchiA explorer |
	buchiA := PlugBuchiExamples
		notAllwaysEventually: [ :env | (env at: #alice) = #dogInYard or: [ (env at: #bob) = #dogInYard ] ].
	explorer := PlugBuchiComposition new.
	explorer
		runtime:
				(PlugBlockLanguageRuntime new
						explorer: explorer;
						program: PlugBlockExamples mutualExclusionLamport83_correct);
		verifier:
				(PlugBuchiVerifier new
						explorer: explorer;
						runtime:
								(PlugBuchiRuntime new
										explorer: explorer;
										buchiAutomaton: buchiA));
		configurationManager: TracingConfigurationManager new;
		transitionManager: FullTransitionManager new;
		explore.
	self assert: explorer verifier propertyViolated not.
	^ explorer