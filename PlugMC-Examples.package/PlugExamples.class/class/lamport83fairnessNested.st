as yet unclassified
lamport83fairnessNested
	"			self lamport83fairnessNested			"
	| explorer |
	explorer := PlugBuchiComposition new.
	explorer
		runtime:
				(PlugBlockLanguageRuntime new
						explorer: explorer;
						program: PlugBlockExamples mutualExclusionLamport83_correct);
		verifier:
				(PlugNestedBuchiVerifier new
						explorer: explorer;
						runtime:
								(PlugBuchiRuntime new
										explorer: explorer;
										buchiAutomaton: self lamport83fairnessBuchi));
		configurationManager: TracingConfigurationManager new;
		transitionManager: FullTransitionManager new;
		explore.
	self assert: explorer verifier propertyViolated.
	^ explorer