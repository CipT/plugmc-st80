as yet unclassified
lamport83BobFlagDownNoCS
	"			self lamport83BobFlagDownNoCS			"
	| explorer |
	explorer := PlugDFSExplorer new.
	explorer
		runtime:
				(PlugBlockLanguageRuntime new
						explorer: explorer;
						program: PlugBlockExamples mutualExclusionLamport83_correct);
		verifier:
				(PlugObserverVerifier new
						explorer: explorer;
						observers: {(PlugObserverExamples bobFlagDownNoCS)};
						runtime: (PlugObserverRuntime new explorer: explorer));
		configurationManager: TracingConfigurationManager new;
		transitionManager: FullTransitionManager new;
		explore.
	self assert: explorer verifier violations isEmpty.
	^ explorer