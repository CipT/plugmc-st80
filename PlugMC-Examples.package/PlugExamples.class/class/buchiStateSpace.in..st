as yet unclassified
buchiStateSpace: explorer in: aGMLFile
	| configurations transitions initial |
	configurations := explorer configurationManager configurations.
	transitions := explorer transitionManager transitions.
	initial := explorer configurationManager initial.
	FileStream
		newFileNamed: aGMLFile
		do: [ :stream | 
			stream
				nextPutAll: 'graph [ directed 1 id 42 label "clock configuration graph"';
				cr.
			stream
				tab;
				nextPutAll:
						'node [ id 0 label "0" graphics [ center [ x 82.0000 y 42.0000 ] w 16.0000 h 16.0000 type "circle" fill "#000000" ]]'.
			configurations
				do: [ :each | 
					stream
						tab;
						nextPutAll: 'node [ id ' , each hash printString , ' label "'.
					stream
						nextPutAll: each systemState printString;
						cr.
					stream nextPutAll: each buchi printString.
					stream nextPutAll: '"'.
					each buchi isAccepting
						ifTrue: [ 
							stream
								nextPutAll: ' graphics [ fill "#AAAAAA" ]]';
								cr ]
						ifFalse: [ 
							stream
								nextPutAll: ']';
								cr ] ].
			stream
				tab;
				nextPutAll: 'edge [ source 0 target ' , initial hash printString , ' label ""]';
				cr.
			transitions
				do: [ :each | 
					stream
						tab;
						nextPutAll: 'edge [ source ' , each key hash printString , ' target ' , each value hash printString , ' label ""]';
						cr ].
			stream nextPutAll: ']' ]