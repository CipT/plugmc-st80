as yet unclassified
coins50
	"			self coins50		"
		
|explorer coinsWeight|
coinsWeight := #(2.3 3.06 3.92 4.1 5.74 7.8 7.5 8.5).
explorer := PlugBFSExplorer new.
explorer
	runtime: (PlugBlockLanguageRuntime new
				explorer: explorer;
				program: (PlugBlockExamples coinProgram) );
	verifier: (PlugPredicateVerifier new
				explorer: explorer;
				predicates: { [ :e | (e with: coinsWeight collect: [ :q :v | q*v  ]) sum ~= 62.5 ] } asSet;
				runtime: (PlugPredicateRuntime new explorer: explorer)
				);
	configurationManager: TracingConfigurationManager new;
	transitionManager: CountingTransitionManager new;
	explore.
	self assert: explorer verifier violations isEmpty not.
	^explorer
