as yet unclassified
implicitCountersPredicateTrue
	"			self implicitCountersPredicateTrue			"

	| explorer |
	explorer := PlugBFSExplorer new.
	explorer
		runtime:
				(PlugBlockLanguageRuntime new
						explorer: explorer;
						program: (PlugBlockExamples implicitCounters: 2 max: 20));
		verifier:
				(PlugPredicateVerifier new
						explorer: explorer;
						predicates: {[ :e | e first ~= 21 ]} asSet;
						runtime: (PlugPredicateRuntime new explorer: explorer));
		configurationManager: TracingConfigurationManager new;
		transitionManager: CountingTransitionManager new;
		explore.
	self assert: explorer verifier violations isEmpty.
	^ explorer