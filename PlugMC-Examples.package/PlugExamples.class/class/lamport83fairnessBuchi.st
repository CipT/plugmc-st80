as yet unclassified
lamport83fairnessBuchi
	^ PlugBuchiExamples
		fairnessAlice: [ :e | e at: #flagAlice ]
		then: [ :e | (e at: #alice) = #dogInYard ]
		bob: [ :e | e at: #flagBob ]
		then: [ :e | (e at: #bob) = #dogInYard ]