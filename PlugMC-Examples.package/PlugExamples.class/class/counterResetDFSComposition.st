as yet unclassified
counterResetDFSComposition
	"			self counterResetDFSComposition		"
|explorer|
explorer := PlugDFSExplorer new.
explorer
	runtime: (PlugBlockLanguageRuntime new 
				explorer: explorer;
				program: PlugBlockExamples counterReset);
	configurationManager: TracingConfigurationManager new;
	transitionManager: CountingTransitionManager new;
	explore.
	^explorer