as yet unclassified
lamport83fairness
	"			self lamport83fairness			"
	| explorer |
	explorer := PlugBuchiComposition new.
	explorer
		runtime:
				(PlugBlockLanguageRuntime new
						explorer: explorer;
						program: PlugBlockExamples mutualExclusionLamport83_correct);
		verifier:
				(PlugBuchiVerifier new
						explorer: explorer;
						runtime:
								(PlugBuchiRuntime new
										explorer: explorer;
										buchiAutomaton: self lamport83fairnessBuchi));
		configurationManager: TracingConfigurationManager new;
		transitionManager: FullTransitionManager new;
		explore.
	self assert: explorer verifier propertyViolated.
	^ explorer