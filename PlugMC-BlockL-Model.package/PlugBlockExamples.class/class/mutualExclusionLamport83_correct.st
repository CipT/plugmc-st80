examples
mutualExclusionLamport83_correct
	"alice & bob"

	| envir aliceI2W  aliceW2D  aliceD2I alice 
	bobI2C bobC2W bobC2D  bobW2C  bobD2I bob |
	envir := {(#flagAlice -> false).
	(#flagBob -> false).
	(#alice -> #idle).
	(#bob -> #idle)} asDictionary.
	aliceI2W := PlugBlockBehavior
		guard: [ :env | (env at: #alice) = #idle ]
		action: [ :env | 
			env at: #alice put: #waiting.
			env at: #flagAlice put: true ].
	aliceW2D := PlugBlockBehavior
		guard: [ :env | (env at: #alice) = #waiting & ((env at: #flagBob) not) ]
		action: [ :env | env at: #alice put: #dogInYard ].
	aliceD2I := PlugBlockBehavior
		guard: [ :env | (env at: #alice) = #dogInYard ]
		action: [ :env | 
			env at: #alice put: #idle.
			env at: #flagAlice put: false ].
	alice := {aliceI2W. aliceW2D. aliceD2I}.
	bobI2C := PlugBlockBehavior
		guard: [ :env | (env at: #bob) = #idle ]
		action: [ :env | 
			env at: #bob put: #checkFlagAlice.
			env at: #flagBob put: true ].
	bobC2W := PlugBlockBehavior
		guard: [ :env | (env at: #bob) = #checkFlagAlice & (env at: #flagAlice) ]
		action: [ :env | 
			env at: #bob put: #waiting.
			env at: #flagBob put: false ].
	bobC2D := PlugBlockBehavior
		guard: [ :env | (env at: #bob) = #checkFlagAlice & ((env at: #flagAlice) not) ]
		action: [ :env | env at: #bob put: #dogInYard ].
	bobW2C := PlugBlockBehavior
		guard: [ :env | (env at: #bob) = #waiting & ((env at: #flagAlice) not) ]
		action: [ :env | 
			env at: #bob put: #dogInYard.
			env at: #flagBob put: true ].
	bobD2I := PlugBlockBehavior
		guard: [ :env | (env at: #bob) = #dogInYard ]
		action: [ :env | 
			env at: #bob put: #idle.
			env at: #flagBob put: false ].
	bob := {bobI2C. bobC2W. bobC2D. bobW2C. bobD2I}.
	^ PlugBlockProgram new
		variables: envir;
		behaviors: alice , bob