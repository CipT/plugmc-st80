examples
counterReset
	| a b |
	a := PlugBlockBehavior guard: [ :env | env first < 5 ] action: [ :env | env at: 1 put: env first + 1 ].
	b := PlugBlockBehavior guard: [ :env | env first >= 5 ] action: [ :env | env at: 1 put: 1 ].
	^ PlugBlockProgram new
		variables: #(1);
		behaviors: {a. b}