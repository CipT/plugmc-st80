as yet unclassified
guard: aGuardBlock action: anActionBlock

	^self new
		guard: aGuardBlock;
		action: anActionBlock.