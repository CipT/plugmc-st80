as yet unclassified
fairnessAlice: aFlagUp then: csA bob: bFlagUp then: csB
	"!(G(p0 -> F q0) && G(p1 -> F q1))"
	|i aA aB i2i i2aA aA2aA i2aB aB2aB buchi|
	i := PlugBuchiState new id: #i.
	aA := PlugBuchiState accepting id: #aAlice.
	aB := PlugBuchiState accepting id: #aBob.
	
	i2i := PlugTTransition from: i to: i guard: [ true ] atoms: {}.
	i2aA := PlugTTransition from: i to: aA guard: [:p :q | p & (q not)] atoms: { aFlagUp. csA }.
	aA2aA := PlugTTransition from: aA to: aA guard: [:p | p not] atoms: { csA }.
	i2aB := PlugTTransition from: i to: aB guard: [:p :q | p & (q not)] atoms: { bFlagUp. csB }.
	aB2aB := PlugTTransition from: aB to: aB guard: [:p | p not] atoms: { csB }.
	
	buchi := PlugTAutomaton new
		initial: i.
	buchi fanout at: i put: { i2i. i2aA. i2aB} asSet.
	buchi fanout at: aA put: { aA2aA } asSet.
	buchi fanout at: aB put: { aB2aB } asSet.
	^buchi