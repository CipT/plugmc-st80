as yet unclassified
notAllwaysEventually: anAtom
	"! (G F p)"
	|i a i2i i2a a2a  buchi|
	i := PlugBuchiState new id: #i.
	a := PlugBuchiState accepting id: #a.
	
	i2i := PlugTTransition from: i to: i guard: [ true ] atoms: {}.
	i2a := PlugTTransition from: i to: a guard: [:p | p not] atoms: {anAtom}.
	a2a := PlugTTransition from: a to: a guard: [:p | p not] atoms: {anAtom}.
	
	buchi := PlugTAutomaton new
		initial: i.
	buchi fanout at: i put: { i2i. i2a} asSet.
	buchi fanout at: a put: { a2a } asSet.
	^buchi