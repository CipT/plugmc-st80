as yet unclassified
fireableTransitionsFrom: aConfiguration to: anotherConfiguration in: observer index: index

	^runtime observer: observer;
		observerIdx: index;
		fireableTransitionsFrom: aConfiguration to: anotherConfiguration runtime: explorer runtime