as yet unclassified
verifyFrom: source to: systemTarget
	| target |
	"the transition in the observer automaton enabled in the new sytem configuration"
	target := systemTarget.
	target observers: (Array new: observers size).
	observers
		withIndexDo: [ :each :idx | 
			target := self
				fireableTransitionsFrom: source
				to: target
				in: each
				index: idx.
			(target observers at: idx) isReject
				ifTrue: [ 
					"once a reject is reached I can remore the observer from the list"
					violations at: (observers at: idx) ifAbsentPut: [ target ] ].
			(target observers at: idx) isAccept
				ifTrue: [ 
					"once an accept is reached I can remore the observer from the list"
					acceptations at: (observers at: idx) ifAbsentPut: [ target ] ].
			"do I handle STOP, STORE, CUT observers here?"
			 ].
	^ target