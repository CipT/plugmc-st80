as yet unclassified
initializeConfiguration: initial
	initial observers: (Array new: observers size).
	observers
		withIndexDo: [ :each :index | 
			initial observers
				at: index
				put:
					(runtime
						observer: each;
						observerIdx: index;
						initialConfiguration) ]