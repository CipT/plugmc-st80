A PlugObserverVerifier is xxxxxxxxx.

Instance Variables
	acceptations:		<Object>
	observers:		<Object>
	violations:		<Object>

acceptations
	- xxxxx

observers
	- xxxxx

violations
	- xxxxx

Observation ideas:
- Fixpoint for observation transitions? Thx Cip&Luka, still not sure though
- Add footer with some booleans, discard it after obs phase
	- cut: destination goes to known without passing by toSee
	- stop: cut + exploration ends after current obs phase
	- store --> no need since the transition manager handles it 