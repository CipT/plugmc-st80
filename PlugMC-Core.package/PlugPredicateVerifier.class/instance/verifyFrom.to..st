as yet unclassified
verifyFrom: source to: target
	"we assert only previously true predicates, the others are already violated"

	predicates
		do: [ :predicate | 
			(self assert: predicate in: target)
				ifFalse: [ 
					predicates remove: predicate.
					violations at: predicate ifAbsentPut: target ] ]