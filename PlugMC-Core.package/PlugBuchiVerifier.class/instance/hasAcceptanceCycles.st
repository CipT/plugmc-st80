as yet unclassified
hasAcceptanceCycles
	| stack |
	stack := Stack new.
	[ acceptingQueue isEmpty ]
		whileFalse: [ 
			| acceptanceConfiguration |
			acceptanceConfiguration := acceptingQueue removeFirst.
			stack push: acceptanceConfiguration.
			[ stack isEmpty ]
				whileFalse: [ 
					| current targets childrenProcessed |
					current := stack top.
					targets := explorer transitionManager fanout: current.
					(targets includes: acceptanceConfiguration)
						ifTrue: [ 
							propertyViolated := true.
							^ true ].
					childrenProcessed := true.
					targets
						do: [ :each | 
							visited
								add: each
								ifAbsent: [ 
									childrenProcessed := false.
									stack push: each ] ].
					childrenProcessed
						ifTrue: [ stack pop ] ] ].
	^ false