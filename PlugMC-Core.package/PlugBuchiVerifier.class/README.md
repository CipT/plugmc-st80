A PlugBuchiVerifier is a verifier checking for acceptance cycles in the synchronous composition of a system and a buchi automata. The composition uses DFS, so it needs the DFS explorer.

Instance Variables
	client:		<PlugDFSExplorer>
	runtime: 	<BuchiLanguageRuntime>
client
	- The state-space explorer

runtime
	- The buchi automata language runtime
