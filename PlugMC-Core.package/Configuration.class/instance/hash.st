as yet unclassified
hash
	|hash|
	hash := self species hash.
	self systemState do: [:e | hash := (hash + e hash) hashMultiply].
	^hash