as yet unclassified
= anotherConfiguration
	self == anotherConfiguration ifTrue: [ ^true ].
	self species == anotherConfiguration species ifFalse: [^ false].
	^systemState = anotherConfiguration systemState 