as yet unclassified
= anotherObsConfiguration
	super = anotherObsConfiguration ifFalse: [ ^false ].
	observers isNil & anotherObsConfiguration observers isNil ifTrue: [ ^true ].
	observers size = anotherObsConfiguration observers size ifFalse: [ ^false ].
	observers with: anotherObsConfiguration observers do: [ :a :b | a = b ifFalse: [ ^false ] ].
	^true