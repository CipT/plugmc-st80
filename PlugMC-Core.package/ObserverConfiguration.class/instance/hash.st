as yet unclassified
hash
	|hash|
	hash := super hash.
	(observers isNil or: [ observers isEmpty ]) ifTrue: [ ^hash ].
	observers do: [ :each | hash := (hash + each hash)  hashMultiply ].
	^ hash