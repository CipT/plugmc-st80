as yet unclassified
fireableTransitionsFrom: aState to: anotherState runtime: languageRuntime
	"this should be used for state-based verification environments (observer, buchi) that need to synchronously advance along the system"
	^self subclassResponsibility 