A ConfigurationManager is responsible for handling the state space. it provides a storage for configurations during explorations.
Subclasses of it might use database storage, complex hashing schemes, or other tricks to enabling exploring as many configuration as possible.
