A PlugNestedBuchiVerifier is a verifier checking for acceptance cycles in the synchronous composition of a system and a buchi automata. The composition uses DFS, so it needs the DFS explorer.

It does so using nested DFS [ 1 ].

[ 1 ] C. Courcoubetis, M. Y. Vardi, P. Wolper, and M. Yannakakis. Memory-efficient algorithms for the verification of temporal properties. Formal Methods in System Design, 1(2/3):275–288, 1992.

other references improvements on this algorithm are: 
[ 2 ] Stefan Schwoon and Javier Esparza. A note on on-the-fly verification algorithms. In Proc. TACAS, LNCS 3440, pages 174–190, 2005.
[ 3 ] A. Gaiser and S. Schwoon. Comparison of Algorithms for Checking Emptiness on Buchi Automata. CoRR, abs/0910.3766, 2009.