as yet unclassified
verifyFrom: source to: systemTarget
	|  targets childrenProcessed |
	"all transition in the buchi automaton enabled in the new sytem configuration"
	childrenProcessed := true.
	targets := self fireableTransitionsFrom: source to: systemTarget.	
	targets
		do: [ :target | 
			explorer transitionManager addFrom: source to: target.
			explorer configurationManager
				add: target
				ifAbsent: [ 
					childrenProcessed := false.
					explorer scheduleForExploration: target ] ].
	^childrenProcessed