as yet unclassified
finished: aConfiguration
	aConfiguration buchi isAccepting
		ifTrue: [ 
			| stack |
			stack := Stack new.
			stack push: aConfiguration.
			[ stack isEmpty ]
				whileFalse: [ 
					| current targets childrenProcessed |
					current := stack top.
					targets := explorer transitionManager fanout: current.
					(targets includes: aConfiguration)
						ifTrue: [ 
							propertyViolated := true.
							^ true ].
					childrenProcessed := true.
					targets
						do: [ :each | 
							visited
								add: each
								ifAbsent: [ 
									childrenProcessed := false.
									stack push: each ] ].
					childrenProcessed
						ifTrue: [ stack pop ] ] ].
	^ false