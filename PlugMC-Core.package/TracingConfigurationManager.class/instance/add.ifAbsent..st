as yet unclassified
add: aConfiguration ifAbsent: aBlock
	configurations ifEmpty: [ initial := aConfiguration ].
	^configurations add: aConfiguration ifAbsent: aBlock
