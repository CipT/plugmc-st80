A TracingConfigurationManager stores the transitions seen during execution, it can be used during exploration or simulation to detect states system states that have been seen already.
This configuration manager does not store the transitions.
The state space is backed by a simple dictionary