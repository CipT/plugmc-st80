as yet unclassified
transitionsFrom: aState to: anotherState
	"this can be used by inspectors, simulators, graphical interfaces, debugging, etc."
	^self shouldBeImplemented 