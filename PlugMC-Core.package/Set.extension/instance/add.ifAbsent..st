*PlugMC-Core
add: newObject ifAbsent: aBlock

	| index object |
	index := self scanFor: newObject.
	(object := array at: index) ifNil: [
		self atNewIndex: index put: newObject asSetElement.
		aBlock value
		].
	^ object
	