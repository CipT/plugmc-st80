as yet unclassified
process: source to: targets
	"for buchi we need to keep the configuration in the stack until all its children are explored"
	"moreover, if one ltl property is falsified we stop the exploration"
	(targets inject: true into: [ :processed :target | processed & (verifier verifyFrom: source to: target) ])
		ifTrue: [ 
			(verifier finished: toSee pop)
				ifTrue: [ finished := true ] ]