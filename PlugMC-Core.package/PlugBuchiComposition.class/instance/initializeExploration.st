as yet unclassified
initializeExploration
	|initial|
	initial := self newConfiguration.
	initial systemState: runtime initialConfiguration.
	verifier initializeConfiguration: initial.
	verifier verifyFrom: initial to: initial deepCopy.
