as yet unclassified
fire: aTarget
	transitionManager addFrom: current to: aTarget.
	configurationManager add: aTarget.
	"we might need to filter blocked transitions. or maybe that should be the responsibility of the language runtime"
	current := aTarget
