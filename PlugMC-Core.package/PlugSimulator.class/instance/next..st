as yet unclassified
next:  aTarget
	transitionManager add: current -> aTarget.
	configurationManager add: aTarget.
	"we might need to filter blocked transitions. or maybe that should be the responsibility of the language runtime"
	current := aTarget.
	^self fireableTransitionsFrom: current