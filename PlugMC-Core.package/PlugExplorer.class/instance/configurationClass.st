accessing
configurationClass
	^ (verifier isKindOf: PlugObserverVerifier)
		ifTrue: [ ObserverConfiguration ]
		ifFalse: [ Configuration ]