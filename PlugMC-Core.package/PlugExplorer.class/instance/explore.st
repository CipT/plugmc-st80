exploration
explore
	self initializeExploration.
	
	[self atEnd] whileFalse: [ self explorationStep ].
	
	self explorationFinished.