exploration
initializeExploration
	|initial|
	initial := self newConfiguration.
	initial systemState: runtime initialConfiguration.
	verifier initializeConfiguration: initial.
	configurationManager add: initial.
	verifier verifyFrom: initial to: initial deepCopy.