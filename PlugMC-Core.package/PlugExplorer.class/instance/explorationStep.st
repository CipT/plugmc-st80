exploration
explorationStep
	|source targets|
	source := self nextConfiguration.
	targets := self fireableTransitionsFrom: source.
	self process: source to: targets.