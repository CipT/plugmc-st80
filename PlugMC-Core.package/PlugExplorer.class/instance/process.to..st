exploration
process: source to: targets
	targets
		do: [ :target | 
			verifier verifyFrom: source to: target.
			transitionManager addFrom: source to: target.
			configurationManager add: target ifAbsent: [ 
				self scheduleForExploration: target ] ].