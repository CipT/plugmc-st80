as yet unclassified
fanout: aConfiguration
	^(transitions select: [ :each | each key = aConfiguration ]) collect: [ :each | each value ]