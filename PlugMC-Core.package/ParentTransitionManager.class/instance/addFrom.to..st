as yet unclassified
addFrom: source to: target 
	"only the first parent found is stored"
	parentMap at: target ifAbsentPut: [ source ]