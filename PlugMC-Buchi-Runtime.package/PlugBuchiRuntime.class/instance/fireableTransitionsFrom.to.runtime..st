as yet unclassified
fireableTransitionsFrom: aState to: anotherState runtime: languageRuntime
	"we look for the transitions in the buchi automaton which are synchronous with the aState->anotherState transition"
	"to achieve it we look for all transitions going out from the source buchi state (aState buchi) for which the guards are true in the target state (anotherState systemState)"
	| transitions |
	transitions := OrderedCollection new.
	buchiAutomaton
		fireableTransitionsIn: aState buchi
		do: [ :fireableTransition | 
			(self isEnabled: fireableTransition in: anotherState systemState runtime: languageRuntime)
				ifTrue: [ 
					| target |
					target := anotherState deepCopy. "since buchi automaton can be nondeterministics we need to copy this"
					target buchi: fireableTransition to.
					transitions add: target ] ].
	^ transitions