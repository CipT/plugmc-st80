as yet unclassified
isEnabled: fireableTransition in: systemState runtime: languageRuntime
	|atomValuations|
	atomValuations := fireableTransition atoms collect: [ :atom | 
		self evaluate: atom in: systemState runtime: languageRuntime].
	^fireableTransition guard valueWithArguments: atomValuations